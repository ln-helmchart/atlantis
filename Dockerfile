FROM runatlantis/atlantis:v0.16.0

RUN curl -LO https://github.com/gruntwork-io/terragrunt/releases/download/v0.26.0/terragrunt_linux_amd64
RUN chmod +x terragrunt_linux_amd64
RUN cp terragrunt_linux_amd64 /usr/local/bin/terragrunt
